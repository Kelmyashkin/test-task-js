function TicTacToe() {
  const self = this;

  self.tableVisual = [[, ,], [, ,], [, ,]];
  self.buttons = { undo: null, redo: null, restart: null };

  self.setEvents = () => {
    self.buttons.undo.addEventListener('click', self.Undo);
    self.buttons.redo.addEventListener('click', self.Redo);
    self.buttons.restart.addEventListener('click', self.Restart);
    self.tableVisual.forEach(row =>
      row.forEach(cell => cell.addEventListener('click', self.CellClick)),
    );
  };

  function isDisable(element) {
    return element.hasAttribute('disabled');
  }

  function changeDisable(element, add) {
    if (add) {
      element.setAttribute('disabled', null);
    } else {
      element.removeAttribute('disabled');
    }
  }

  function isEqual(v1, v2, v3) {
    return v1 === v2 && v2 === v3 && v1 !== null;
  }

  function addClass(element, classes) {
    const newList = classes.split(' ');
    const oldList = element.className.split(' ');

    for (let i = 0; i < newList.length; i++) {
      const c = newList[i];

      if (oldList.indexOf(c) === -1) {
        oldList.push(c);
      }
    }

    element.className = oldList.join(' ');
  }

  self.updateVisual = () => {
    for (let i = 0; i < self.table.length; i++) {
      for (let j = 0; j < self.table[i].length; j++) {
        const element = self.tableVisual[i][j];
        const value = self.table[i][j];
        if (value == null) {
          if (element.className !== 'cell') {
            element.className = 'cell';
          }
        } else {
          const className = self.table[i][j] ? 'ch' : 'r';
          if (element.className.indexOf(className) == -1) {
            element.className += ` ${className}`;
          }
        }
      }
    }

    changeDisable(self.buttons.undo, !self.moves.length || self.isEnd);
    changeDisable(self.buttons.redo, !self.undoMoves.length || self.isEnd);

    const resultTitle = document.getElementsByClassName('won-title')[0];
    if (self.isEnd) {
      resultTitle.className = 'won-title';

      document.getElementsByClassName('won-message')[0].innerHTML = self.winner
        ? 'Crosses won!'
        : self.winner == null ? "It's a draw!" : 'Toes won!';

      console.log(self.result);

      const keys = Object.keys(self.result);
      const v = self.tableVisual;
      for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        switch (key) {
          case 'row':
            {
              v[self.result.row].forEach(cell =>
                addClass(cell, 'win horizontal'),
              );
            }
            break;
          case 'column':
            {
              [
                v[0][self.result.column],
                v[1][self.result.column],
                v[2][self.result.column],
              ].forEach(cell => addClass(cell, 'win vertical'));
            }
            break;
          case 'diagonal':
            {
              for (let i = 0; i < self.result.diagonal.length; i++) {
                const direction = self.result.diagonal[i];
                if (direction == 'main') {
                  [v[0][0], v[1][1], v[2][2]].forEach(cell =>
                    addClass(cell, 'win diagonal-right'),
                  );
                } else {
                  [v[0][2], v[1][1], v[2][0]].forEach(cell =>
                    addClass(cell, 'win diagonal-left'),
                  );
                }
              }
            }
            break;
        }
      }
    } else {
      if (resultTitle.className !== 'won-title hidden')
        resultTitle.className = 'won-title hidden';
    }
  };

  self.Restart = () => {
    self.table = [[null, null, null], [null, null, null], [null, null, null]];
    self.currentTurn = true; // true - is cross move, false - is round move
    self.moves = [];
    self.undoMoves = [];
    self.isEnd = false;
    self.result = null;
    self.winner = null;

    self.updateVisual();
  };

  self.Undo = e => {
    if (!isDisable(e.target)) {
      const move = self.moves.pop();
      self.table[move.row][move.cell] = null;
      self.undoMoves.push(move);
      self.currentTurn = !self.currentTurn;
      self.updateVisual();
    }
  };

  self.Redo = e => {
    if (!isDisable(e.target)) {
      const move = self.undoMoves.pop();
      self.table[move.row][move.cell] = move.value;
      self.moves.push(move);
      self.currentTurn = !self.currentTurn;
      self.updateVisual();
    }
  };

  self.CellClick = e => {
    if (self.isEnd) return false;

    const cell = e.target;
    const row = cell.parentElement;
    const rowIndex = Array.from(row.parentElement.children).indexOf(row);
    const cellIndex = Array.from(row.children).indexOf(cell);

    if (self.table[rowIndex][cellIndex] == null) {
      self.moves.push({
        row: rowIndex,
        cell: cellIndex,
        value: self.currentTurn,
      });
      self.undoMoves = [];

      self.table[rowIndex][cellIndex] = self.currentTurn;
      self.currentTurn = !self.currentTurn;

      self.checkEnd();

      self.updateVisual();
    }
  };

  self.checkEnd = () => {
    const result = {};
    const t = self.table;

    //row case
    for (let row = 0; row < t.length; row++) {
      const r = t[row];
      if (isEqual(r[0], r[1], r[2])) {
        result.row = row;
        self.winner = r[0];
        break;
      }
    }

    //column case
    for (let column = 0; column < 3; column++) {
      if (isEqual(t[0][column], t[1][column], t[2][column])) {
        result.column = column;
        self.winner = t[0][column];
        break;
      }
    }

    //diagonal case
    const diagonal = [];
    if (isEqual(t[0][0], t[1][1], t[2][2])) {
      diagonal.push('main');
      self.winner = t[0][0];
    }
    if (isEqual(t[0][2], t[1][1], t[2][0])) {
      diagonal.push('anti');
      self.winner = t[0][2];
    }
    if (diagonal.length) result.diagonal = diagonal;

    if (!t.some(row => row.some(cell => cell === null))) {
      result.draw = true;
      self.winner = null;
    }

    const keys = Object.keys(result);
    if (keys.length) {
      self.isEnd = true;
      self.result = result;
    }
  };

  self.Init = () => {
    self.buttons.undo = document.getElementsByClassName('undo-btn')[0];
    self.buttons.redo = document.getElementsByClassName('redo-btn')[0];
    self.buttons.restart = document.getElementsByClassName('restart-btn')[0];
    Array.from(document.getElementsByClassName('row')).forEach(
      (row, rowIndex) => {
        Array.from(row.getElementsByClassName('cell')).forEach(
          (cell, cellIndex) => {
            self.tableVisual[rowIndex][cellIndex] = cell;
          },
        );
      },
    );

    self.setEvents();
    self.Restart();
  };

  return self;
}

document.addEventListener('DOMContentLoaded', function() {
  const ticTacToe = new TicTacToe();
  ticTacToe.Init();
});
